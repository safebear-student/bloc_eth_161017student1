Web3 = require('web3');
fs = require('fs');

web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
console.log(web3.eth.accounts);

abiDefinition = JSON.parse(fs.readFileSync('out/Safevote.abi').toString());
VotingContract = web3.eth.contract(abiDefinition);
byteCode = fs.readFileSync('out/Safevote.bin');
deployedContract = VotingContract.new(['Simon', 'George', 'Elinor'],
    {    data : byteCode, from: web3.eth.accounts[0], gas:4700000 })

console.log('address', deployedContract.address);

contractInstance = VotingContract.at(deployedContract.address);

console.log('Simon before', contractInstance.totalVotesFor.call('Simon'));

contractInstance.voteForCandidate('Simon', {from:web3.eth.accounts[0]})
contractInstance.voteForCandidate('Simon', {from:web3.eth.accounts[0]})
contractInstance.voteForCandidate('Simon', {from:web3.eth.accounts[0]})

console.log('Simon after', contractInstance.totalVotesFor.call('Simon'));